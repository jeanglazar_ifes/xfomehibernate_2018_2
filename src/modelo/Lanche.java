/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;


/**
 *
 * @author 1547816
 */

@Entity
public class Lanche implements Serializable {
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private int idLanche;
    
    private String nome;
    
    @Column(precision = 2)
    private float valor;
    
    private String ingredientes;

    @ManyToMany( fetch = FetchType.LAZY )
    @JoinTable ( name = "pedido_lanche" ,
                 joinColumns = @JoinColumn(name="idLanche") ,
                 inverseJoinColumns = @JoinColumn(name="idPedido") )
    List<Pedido> pedidos = new ArrayList();;

    // Construtor VAZIO para o HIBERNATE
    public Lanche() {
    }

    public Lanche(String nome, float valor, String ingredientes) {
        this.nome = nome;
        this.valor = valor;
        this.ingredientes = ingredientes;
    }

    public Lanche(int idLanche, String nome, float valor, String ingredientes) {
        this.idLanche = idLanche;
        this.nome = nome;
        this.valor = valor;
        this.ingredientes = ingredientes;
    }

    public int getIdLanche() {
        return idLanche;
    }

    public void setIdLanche(int idLanche) {
        this.idLanche = idLanche;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    @Override
    public String toString() {
        return nome;
    }


    
    
}
