/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.*;


/**
 *
 * @author 1547816
 */

@Entity
public class Pedido implements Serializable {
   
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private int idPedido;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date data; 
    
    private String observacao;
    private int maisBife;
    private int maisBacon;
    private int maisPresunto;
    private int maisQueijo;
    private char entregar;
    
    @Column(precision = 2)
    private float valorTotal;

    @ManyToOne(fetch = FetchType.EAGER)
    private Cliente cliente;
    
    @ManyToMany( fetch = FetchType.LAZY )
    @JoinTable ( name = "pedido_lanche" ,
                 joinColumns = @JoinColumn(name="idPedido") ,
                 inverseJoinColumns = @JoinColumn(name="idLanche") )
    List<Lanche> lanches = new ArrayList();

    // Construtor VAZIO para o HIBERNATE
    public Pedido() {
    }

    public Pedido(Date data, String observacao, int maisBife, int maisBacon, int maisPresunto, int maisQueijo, char entregar, float valorTotal, Cliente cliente) {
        this.data = data;
        this.observacao = observacao;
        this.maisBife = maisBife;
        this.maisBacon = maisBacon;
        this.maisPresunto = maisPresunto;
        this.maisQueijo = maisQueijo;
        this.entregar = entregar;
        this.valorTotal = valorTotal;
        this.cliente = cliente;
    }

    public Pedido(int idPedido, Date data, String observacao, int maisBife, int maisBacon, int maisPresunto, int maisQueijo, char entregar, float valorTotal, Cliente cliente, List<Lanche> lanches) {
        this.idPedido = idPedido;
        this.data = data;
        this.observacao = observacao;
        this.maisBife = maisBife;
        this.maisBacon = maisBacon;
        this.maisPresunto = maisPresunto;
        this.maisQueijo = maisQueijo;
        this.entregar = entregar;
        this.valorTotal = valorTotal;
        this.cliente = cliente;
        this.lanches = lanches;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public Date getData() {
        return data;
    }

    public String getDataStr() {
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        return formato.format(data);
    }
    public void setData(Date data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getMaisBife() {
        return maisBife;
    }

    public void setMaisBife(int maisBife) {
        this.maisBife = maisBife;
    }

    public int getMaisBacon() {
        return maisBacon;
    }

    public void setMaisBacon(int maisBacon) {
        this.maisBacon = maisBacon;
    }

    public int getMaisPresunto() {
        return maisPresunto;
    }

    public void setMaisPresunto(int maisPresunto) {
        this.maisPresunto = maisPresunto;
    }

    public int getMaisQueijo() {
        return maisQueijo;
    }

    public void setMaisQueijo(int maisQueijo) {
        this.maisQueijo = maisQueijo;
    }

    public char getEntregar() {
        return entregar;
    }

    public void setEntregar(char entregar) {
        this.entregar = entregar;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Lanche> getLanches() {
        return lanches;
    }

    public void setLanches(List<Lanche> lanches) {
        this.lanches = lanches;
    }

    @Override
    public String toString() {
        return String.valueOf( idPedido );
    }
  
    
    public Object[] toArray() {   
        
        DateFormat formato = new SimpleDateFormat("d/M/y");
        NumberFormat formNum = NumberFormat.getCurrencyInstance();
        
        /*
        DecimalFormat formNum = new DecimalFormat();
        formNum.setMaximumFractionDigits(2);
        formNum.setMinimumFractionDigits(2);
        */
        
        return new Object[] {this, cliente, lanches, cliente.getBairro(), 
                             formato.format(data), 
                             formNum.format(valorTotal)  };        
    }
}
