/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;



/**
 *
 * @author 1547816
 */

@Entity
public class Cliente implements Serializable {
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private int idCliente;
    
    @Column (name="nome", nullable = false, length = 150)
    private String nome;
    
    private String endereco;
    private int numero;
    private String complemento;
    private String bairro;
    private String referencia;
    
    @Column (length = 14)
    private String telefone;
    
    @Column (length = 14)   // (27)99876-1234
    private String celular;
    
    @Column (unique = true)
    private String email;
    
    @Lob
    private byte[] foto;
    
    @ManyToOne( fetch = FetchType.EAGER )
    @Cascade(CascadeType.SAVE_UPDATE)
    private Cidade cidade;

    @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY )
    private List<Pedido> pedidos;
   
    // TODOS OS CAMPOS
    public Cliente(int idCliente, String nome, String endereco, int num, String complemento, String bairro, String referencia, String tel, String celular, String email, Cidade cidade, byte[] foto) {
        this.idCliente = idCliente;
        this.nome = nome;
        this.endereco = endereco;
        this.numero = num;
        this.complemento = complemento;
        this.bairro = bairro;
        this.referencia = referencia;
        this.telefone = tel;
        this.celular = celular;
        this.email = email;
        this.cidade = cidade;
        this.foto = foto;                
    }

    // NENHUM CAMPO
    public Cliente() {
    }

    // SEM O ID AUTO NUMERAÇÃO
    public Cliente(String nome, String endereco, int num, String complemento, String bairro, String referencia, String tel, String celular, String email, Cidade cidade, byte[] foto) {
        this.nome = nome;
        this.endereco = endereco;
        this.numero = num;
        this.complemento = complemento;
        this.bairro = bairro;
        this.referencia = referencia;
        this.telefone = tel;
        this.celular = celular;
        this.email = email;
        this.cidade = cidade;
        this.foto = foto;  
        this.idCliente = -1;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }


 
    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
  
    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }
    
    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }
   
    public Object[] toArray() {
        
        return new Object[] {this, bairro, cidade, telefone, celular};
        
    } 

    @Override
    public String toString() {
        return idCliente + " - " + nome;
    }

 

}
