/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Cliente;
import modelo.Pedido;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.ResultTransformer;

/**
 *
 * @author 1547816
 */
public class PedidoDAO extends GenericDAO {

    
    public PedidoDAO()  {
           
    }
    
    private List<Pedido> pesquisar(int tipo, String pesq) throws SQLException {
              
        List<Pedido> lista = new ArrayList();
        Session sessao = ConexaoHibernate.getSessionFactory().openSession();
        sessao.getTransaction().begin();
        
        // 1) CRIAR o CRITERIA
        Criteria consulta = sessao.createCriteria( Pedido.class );
        consulta.setResultTransformer( Criteria.DISTINCT_ROOT_ENTITY  );
        
        // 2) ASSOCIAÇÕES
        consulta.createAlias("cliente", "cli");      
        consulta.createAlias("lanches", "lan", JoinType.LEFT_OUTER_JOIN );      
           
        // 3) CRITERIOS de pesquisa                        
        switch ( tipo ) {
            case 0: consulta.add( Restrictions.eq("idPedido", Integer.parseInt(pesq) ) );
                    break;
            case 1: consulta.add( Restrictions.like("cli.nome", pesq + "%") );
                    break;                    
            case 2: consulta.add( Restrictions.like("lan.nome", pesq + "%") );
                    break;                                        
            case 3: consulta.add( Restrictions.like("cli.bairro", pesq + "%") );
                    break;                                        
            case 4: 
                    String vetor[] = pesq.split("/");
                    consulta.add( Restrictions.sqlRestriction(" MONTH(data) = " + vetor[0] + " AND YEAR(data) = " + vetor[1]) );
                    break;                                        
                    
        }
        
        // 4) ORDENAÇÃO
        consulta.addOrder( Order.asc("cli.nome") );
        consulta.addOrder( Order.asc("idPedido") );
        
        
        // 5) EXECUTA A PESQUISA
        lista = consulta.list();
        
        sessao.getTransaction().commit();
        sessao.close();
        return lista;               
    } 

    public List<Pedido> pesquisarPorID(String pesq) throws SQLException {
         return pesquisar(0,pesq);             
    }
    
    public List<Pedido> pesquisarPorCliente(String pesq) throws SQLException {
       return pesquisar(1,pesq);                
    }
    
    public List<Pedido> pesquisarPorLanche(String pesq) throws SQLException {
       return pesquisar(2,pesq);                
    }
    public List<Pedido> pesquisarPorBairro(String pesq) throws SQLException {
       return pesquisar(3,pesq);                
    }
    public List<Pedido> pesquisarPorMes(String pesq) throws SQLException {
       return pesquisar(4,pesq);                
    }        
     
}
