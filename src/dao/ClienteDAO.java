/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.Cidade;
import modelo.Cliente;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author 1547816
 */
public class ClienteDAO extends GenericDAO {

    
    public ClienteDAO()  {
           
    }
    
    private List<Cliente> pesquisar(int tipo, String pesq) throws SQLException {
              
        List<Cliente> lista = new ArrayList();
        Session sessao = ConexaoHibernate.getSessionFactory().openSession();
        sessao.getTransaction().begin();
        
        Criteria consulta = sessao.createCriteria( Cliente.class );
        
        switch ( tipo ) {
            case 1: consulta.add( Restrictions.like("nome", pesq + "%") );
                    break;
            case 2: consulta.add( Restrictions.like("bairro", pesq + "%") );
                    break;                    
        }
        
        
        lista = consulta.list();
        
        sessao.getTransaction().commit();
        sessao.close();
        return lista;               
    } 
                
    public List<Cliente> pesquisarNome(String pesq) throws SQLException {
         return pesquisar(1,pesq);     
        
    }
    
    public List<Cliente> pesquisarBairro(String pesq) throws SQLException {
       return pesquisar(2,pesq);                
    }
        
     
}
