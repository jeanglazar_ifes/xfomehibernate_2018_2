/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.ClienteDAO;
import dao.GenericDAO;
import dao.PedidoDAO;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Cidade;
import modelo.Cliente;
import modelo.Lanche;
import modelo.Pedido;

/**
 *
 * @author 1547816
 */
public class CtrlDominio {
    
    ClienteDAO cliDao;
    GenericDAO genDao;
    PedidoDAO pedDao;
    
    CtrlRelatorios ctrlRel;
    
    public CtrlDominio() throws ClassNotFoundException, SQLException {
        cliDao = new ClienteDAO();        
        genDao = new GenericDAO();        
        pedDao = new PedidoDAO();        
        ctrlRel = new CtrlRelatorios();
    }
    
    public void preencherCombo ( JComboBox combo, Class classe) {
        List lista;
        try {
            lista = genDao.listar( classe );
            combo.setModel(  new DefaultComboBoxModel(lista.toArray() )   );
        } catch (SQLException ex) {
            combo = null;
        }
  
    }
        
    
    public int inserirCliente(String nome, String endereco, int num, String complemento, String bairro, String referencia, String tel, String celular, String email, Cidade cidade, Icon foto) throws SQLException, ClassNotFoundException {
        
        Cliente cli = new Cliente(nome, endereco, num, complemento, bairro, referencia, tel, celular, email, cidade, IconToBytes(foto) );
        cliDao.inserir(cli);
        return cli.getIdCliente();
        
    }
    
    
    public int alterarCliente(int id, String nome, String endereco, int num, String complemento, String bairro, String referencia, String tel, String celular, String email, Cidade cidade, Icon foto) throws SQLException, ClassNotFoundException {
        
        Cliente cli = new Cliente(id, nome, endereco, num, complemento, bairro, referencia, tel, celular, email, cidade, IconToBytes(foto) );
        cliDao.alterar(cli);
        return cli.getIdCliente();
        
    }
    
    public void excluirCliente (Cliente cli) throws SQLException {
        cliDao.excluir(cli);
    }
    
    public void pesquisarCliente (int tipo, String pesq, JTable tabela) {
        
        List<Cliente> lista = null;
        try {
            
            if ( tabela != null ) {
                // ZERAR a TABLE
                ( (DefaultTableModel) tabela.getModel() ).setRowCount(0);
            }
                
            switch (tipo) {
                case 0 :          
                    // PEsquisar por nome
                    lista = cliDao.pesquisarNome(pesq);
                    break;

                case 1: // Pesquisar por ID
                    break;

                case 2: // Pesquisar por bairro
                    lista = cliDao.pesquisarBairro(pesq);
                    break;
            }
                        
            if ( lista == null || lista.size() == 0 ){
                JOptionPane.showMessageDialog(null, "Cliente não encontrado." , "INFO Cliente", JOptionPane.INFORMATION_MESSAGE);
            } else {
                
                if ( tabela != null ) {
                    // PREENCHER TABELA
                    for ( Cliente cli : lista  ) {                   
                        ( (DefaultTableModel) tabela.getModel() ).addRow( cli.toArray()  );
                    }                
                } else {
                    // MOSTRAR RELATORIO
                    ctrlRel.relClientes(lista);            
                }
            }
        
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO ao pesquisar CLIENTE. " + ex.getMessage() , 
                    "ERRO Cliente", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void pesquisarPedido(int tipo, String pesq, JTable tabela) {
        
        List<Pedido> lista = null;
        try {
            // ZERAR a TABLE
            ( (DefaultTableModel) tabela.getModel() ).setRowCount(0);

            switch (tipo) {
                case 0 :          
                    // PEsquisar por ID
                    lista = pedDao.pesquisarPorID(pesq);
                    break;

                case 1: // Pesquisar por CLiente
                    lista = pedDao.pesquisarPorCliente(pesq);
                    break;

                case 2: // Pesquisar por bairro
                    lista = pedDao.pesquisarPorLanche(pesq);
                    break;
                    
                case 3: // Pesquisar por bairro
                    lista = pedDao.pesquisarPorBairro(pesq);
                    break;
                
                case 4: // Pesquisar por bairro
                    lista = pedDao.pesquisarPorMes(pesq);
                    break;                    
            }
                        
            if ( lista == null || lista.size() == 0 ){
                JOptionPane.showMessageDialog(null, "Cliente não encontrado." , "INFO Cliente", JOptionPane.INFORMATION_MESSAGE);
            } else {
                
                for ( Pedido ped : lista  ) {                   
                    ( (DefaultTableModel) tabela.getModel() ).addRow( ped.toArray()  );
                }                
            }
        
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERRO ao pesquisar CLIENTE. " + ex.getMessage() , 
                    "ERRO Cliente", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public int inserirPedido(Cliente cliente, int bife, int queijo, int presunto,
            int ovo, char entregar, JTable tabLanches) {
                                                                           
        Pedido ped = new Pedido(new Date(), "", bife, ovo, presunto, queijo, entregar, 0, cliente);

        // Percorrer a tabela de Lanches e insere em Pedido
        List<Lanche> lista = ped.getLanches();
        Lanche lan;
        float valorTotal = 0;

        int tam = tabLanches.getRowCount();

        if (tam > 0) {
            for (int i = 0; i < tam; i++) {
                lan = (Lanche) tabLanches.getValueAt(i, 1);
                lista.add(lan);
                valorTotal += lan.getValor();
            }

            ped.setValorTotal(valorTotal);

            genDao.inserir(ped);
            return ped.getIdPedido();
        } else {
            return -1;
        }

    }
    
    public void listarCliente () {
        
    }

    private byte[] IconToBytes(Icon icon) {
        if ( icon == null ) {
            return null;
        }
        BufferedImage img = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = img.createGraphics();
        icon.paintIcon(null, g2d, 0, 0);
        g2d.dispose();

        byte[] bFile = null;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageOutputStream ios = ImageIO.createImageOutputStream(baos);
            try {
                ImageIO.write(img, "png", ios);
                // Set a flag to indicate that the write was successful
            } finally {
                ios.close();
            }
            bFile = baos.toByteArray();
        } catch (IOException ex) {
            bFile = null;
        }
        finally {
            return bFile;
        } 

    }    
}
