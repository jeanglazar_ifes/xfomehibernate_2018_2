/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Color;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modelo.Cliente;
import visao.DlgCadCliente;
import visao.DlgCadPedidos;
import visao.DlgClientePesq;
import visao.DlgFerramentas;
import visao.DlgPedidoPesq;
import visao.FrmPrincipal;

/**
 *
 * @author 1547816
 */
public class CtrlIHC {
    
    private FrmPrincipal frmPrinc;
    private DlgCadCliente dlgCadCli;
    private DlgClientePesq dlgPesqCli;
    private DlgCadPedidos janPedidos;
    private DlgPedidoPesq janPesqPedidos;
    private DlgFerramentas janFer;
    
    private CtrlDominio ctrlDominio;
    
    private Color cor;
    
    public CtrlIHC() {
        try {
            frmPrinc = new FrmPrincipal(this);
            ctrlDominio = new CtrlDominio();
        } catch (ClassNotFoundException erro) {
            JOptionPane.showMessageDialog(null, "Classe de Drivers não encontrada.",
                    "ERRO de conexão", JOptionPane.ERROR_MESSAGE );
            System.exit(1234);
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, "ERRO de conexão. " + erro.getMessage() ,
                    "ERRO de conexão", JOptionPane.ERROR_MESSAGE );
            System.exit(1299);
        }
        
        
    }

    public CtrlDominio getCtrlDominio() {
        return ctrlDominio;
    }
    
    
    
    public void janelaPrincipal() {        
        frmPrinc.setVisible(true);
        
    }
    
    public void janelaCadCliente() {  
        dlgCadCli = new DlgCadCliente( frmPrinc, true, this );
        dlgCadCli.setVisible(true);
    }
    
    public Cliente janelaPesqCliente() {  
        dlgPesqCli = new DlgClientePesq( frmPrinc, true, this );
        dlgPesqCli.setVisible(true);
        
        return dlgPesqCli.getCliSelecionado();
    }
    
    public void janelaPedidos() { 
        janPedidos = new DlgCadPedidos(frmPrinc, true, this);  
        janPedidos.setVisible(true); 
    }
    
    public void janelaPesqPedidos() { 
        janPesqPedidos = new DlgPedidoPesq(frmPrinc, true, this);  
        janPesqPedidos.setVisible(true); 
    }
    
    public void janelaFerramentas() { 
        janFer = new DlgFerramentas(frmPrinc, true, this);  
        janFer.setVisible(true); 
        cor = janFer.getCor();
    }

    public Color getCor() {
        return cor;
    }
    
    
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        CtrlIHC ctrlIHC = new CtrlIHC();
        ctrlIHC.janelaPrincipal();
    }
    
}
